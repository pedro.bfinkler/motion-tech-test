require("dotenv").config();
import axios from "axios";
import { FacebookApiResponse } from "./types";

// Constants
const ACCESS_TOKEN = process.env.ACCESS_TOKEN;
const API_LIMIT_THRESHOLD = 100;
const FACEBOOK_API_URL = `https://graph.facebook.com/v19.0/me?fields=id,name,last_name&access_token=${ACCESS_TOKEN}`;

if (!ACCESS_TOKEN) {
  throw new Error("ACCESS_TOKEN is required");
}

async function getFacebookFieldsData() {
  const requestTimer = setInterval(async () => {
    try {
      const response = await axios.get<FacebookApiResponse>(FACEBOOK_API_URL);

      console.log(response.data);

      if (hasExceededRateLimit(response.headers["x-app-usage"])) {
        console.log("You have reached the limit of the API");
        clearInterval(requestTimer);
      }
    } catch (error: unknown) {
      if (axios.isAxiosError(error)) {
        const response = error.response;
        const facebookError = (response?.data as FacebookApiResponse).error;

        // It's also possible to handle errors based on the facebook error code (https://developers.facebook.com/docs/graph-api/guides/error-handling)
        console.log(
          `Error code: ${facebookError?.code}. ${facebookError?.message}`
        );
      } else {
        console.log("Unexpected error:", error);
      }

      clearInterval(requestTimer);
    }
  }, 2000);
}

/**
 * This function could be a simple ternary for checking if the limit was exceeded or not,
 * but we wouldn't be able to tell exactly what limit was reached.
 *
 * I chose this approach to be able to tell exactly what limit was reached and make adjustments to the app accordingly.
 *
 * This can also be tweaked to prevent the API from reaching a limit lower than 100.
 */
function hasExceededRateLimit(appUsageHeader: {
  call_count: number;
  total_cputime: number;
  total_time: number;
}): boolean {
  if (appUsageHeader.call_count >= API_LIMIT_THRESHOLD) {
    console.log("You have exceeded the call count limit of the API");
    return true;
  }
  if (appUsageHeader.total_cputime >= API_LIMIT_THRESHOLD) {
    console.log("You have exceeded the total CPU limit of the API");
    return true;
  }
  if (appUsageHeader.total_time >= API_LIMIT_THRESHOLD) {
    console.log("You have exceeded the total time limit of the API");
    return true;
  }

  return false;
}

getFacebookFieldsData();
