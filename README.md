# Facebook API Technical Test

This project is a simple application that uses the Facebook API to fetch user data. This was done as part of the Motion selection process.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have Node.js installed on your machine. This project was built using Node.js version 18.20.0 and npm version 10.5.0.

### Installing

1. Clone the repository
2. Install the dependencies: `npm install`

### Configuration

The project uses environment variables for configuration. Copy the `.development.env` file to `.env` and replace the placeholders with your actual data.

### Running the Application

To run the application, use the following command: `npm run test`

## Built With

- [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
- [dotenv](https://github.com/motdotla/dotenv) - Module that loads environment variables from a .env file

## Author

- **Pedro Bacchi Finkler**