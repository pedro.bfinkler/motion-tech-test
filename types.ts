export interface FacebookApiResponse {
  data: any;
  error?: {
    message: string;
    type: string;
    code: number;
    fbtrace_id: string;
  };
}
